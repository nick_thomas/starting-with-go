package main
import "fmt"

func main() {
	var year = 2100
	fmt.Println("The year is ",year, "should you leap?")
	var leap = year %400 == 0 || (year %4 == 0 && year%100 !=0)

	if leap {
		fmt.Println("Look before you leap!")
	} else{
		fmt.Println("Keep your feet on the ground")
	}
}
