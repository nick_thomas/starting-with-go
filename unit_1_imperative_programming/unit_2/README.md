**Does Go allow direct comparison of text with numbers?**

No

---

**How to use multiple conditions in switch statement?**

You can use them in the same line as below

```
case "enter cave","go inside":
	fmt.Println("You find yourself in a dimly lit cavern.")
```

---

**How to execute the body of the next case?**

use *fallthrough* keyword within switch to execute it.

```
	case room =="lake":
		fmt.Println("The ice seems solid enough.")
		fallthrough
```

---

**How to use infinite loop?**

A *for* loop which doesn't specifiy any break conditions

```
for {
/* code */
}
```

---

**How to break out of an infinite loop?**

Use the *break*  command.

---
