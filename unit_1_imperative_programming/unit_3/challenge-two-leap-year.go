package main

import (
	"fmt"
	"math/rand"
)

func main() {
	for i := 0; i < 10; i++ {

		year := rand.Intn(50_00) + 1
		month := rand.Intn(12) + 1
		daysInMonth := 31

		switch month {
		case 2:
			if year%4 == 0 {
				fmt.Println("Leap year!")
				daysInMonth = 29
			} else {
				daysInMonth = 28
			}
		case 4, 6, 9, 11:
			daysInMonth = 30
		}
		day := rand.Intn(daysInMonth) + 1
		fmt.Println(year, month, day)
	}
}
