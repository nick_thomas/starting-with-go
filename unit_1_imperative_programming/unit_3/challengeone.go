package main

import (
	"fmt"
	"math/rand"
	"time"
)

// “Use October 13, 2020 as the departure date for all tickets. Mars will be 62,100,000 km away from Earth at the time.

//      Randomly choose the speed the ship will travel, from 16 to 30 km/s. This will determine the duration for the trip to Mars
//         and also the ticket price. Make faster ships more expensive, ranging in price from $36 million to $50 million. Double the
//         price for round trips.”

func main() {
	fmt.Println("Spaceline      Days Trip type Prices")
	print_flight()
	fmt.Println("===================================")
}

func get_parsed_time() (time.Time, error) {

	startDate := "October 13, 2020"
	layout := "October 13, 2020"
	parsed, err := time.Parse(layout, startDate)
	if err != nil {
		return time.Time{}, err
	}
	return parsed, nil
}

func print_flight() {
	var distance = 62_100_000

	for count := 0; count < 10; count += 1 {
		total_hours := distance / (rand.Intn(31) * 3600)
		days := total_hours / 24
		var cost = 0
		if days < 60 {
			cost = 40 + rand.Intn(50-40+1)
		} else {

			cost = 30 + rand.Intn(50-40+1)
		}
		flight := ""
		switch num := rand.Intn(3); num {
		case 0:
			flight = "Virgin Galactic"
		case 1:
			flight = "Space X"
		case 2:
			flight = "Space X Adventures"
		default:
			flight = "ISRO"

		}
		fmt.Println(flight, days, "Round-trip", "$", cost)
	}
}
