**Does Go support scope?**

Yes

---

**What happens when a variable is no longer visible or accessible?**

The compiler responds with an `undefined`

---

**How does short declaration/ alternative syntax for the var keyword look?**

`:=` is used
var count = 10 is same as count := 10

---

**What is package scope?**

Variable declared outside of the main function, is visibile for multiple functions.

---

**How to create a new scope?**

Using braces `{}`

---

**Do `case` and `default` introduce a new scope?**

Yes

---

**Are variables declared on the same line as for,if or switch, in scope until the end of the statement?**

Yes

---

**What is short declaration?**

It provides an alternative syntax for the var keyword

```
var count = 10
count := 10”
```

**How to know if the variables are scoped too tightly?**

Code is being duplicated due to where variables are declared
