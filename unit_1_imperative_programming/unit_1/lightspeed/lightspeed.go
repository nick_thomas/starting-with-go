package main

import "fmt"

func main() {
	const lightspeed = 299792
	const hoursPerDay = 24
	var (
		distance = 5600_00_00
		speed = 100800
	)

	fmt.Println(distance/lightspeed, "seconds")

	distance = 40100_00_00
	fmt.Println(distance/lightspeed, "seconds")
	fmt.Println(distance/speed/hoursPerDay,"days")
}
