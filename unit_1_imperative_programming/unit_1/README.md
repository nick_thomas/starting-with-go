#Unit 1

**What is Go?**

Go is a compiled programming language.

---

**Is Go strict about placement of curly braces?**

Yes

---

**What is the first arguement of Printf?**

The first arguement of Printf is always text, unlike Println/Print, which contains *format verb*, example: %v

---

**What is the difference between Printf and Println function?**

Println function automatically moves to the next line, but Printf/Print don't

---

**How to align text using Printf?**

Specify a width as part of the format verb such as `%4v`, pads a value to a width of 4 characters

---

**Can variables be declared before using them in Go?**

No

---

**How to create a constant in Go?**

Using the keyword `const`

---

*How to declare multiple related values in Go?*

```
var (
distance = 5600_00_00
speed = 100800
)
or
var distance,speed = 5600_00_00,100800
```

---

**Does Go support prefix increment?**

No. Supported operations are
*=1
+=1
x++
/=2

---
