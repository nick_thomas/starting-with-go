package threeindexslice

import (
	slicedump "lesson18/sliceDump"
)

func ThreeIndexSlice() {
	planets := []string{"Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune"}
	terrestrial := planets[0:4:4]
	worlds := append(terrestrial, "Ceres")
	slicedump.SliceDump("Original Planet", planets)
	slicedump.SliceDump("Terrerstial Planet", terrestrial)
	slicedump.SliceDump("Secondary world", worlds)
}
