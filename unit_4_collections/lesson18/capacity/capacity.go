package capacity

import "fmt"

// “Write a program that uses a loop to continuously append an element to a slice. Print out the capacity of the slice whenever
// it changes. Does append always double the capacity when the underlying array runs out of room?

func Capacity() {
	item := []string{"test", "some"}
	for i := 0; i < 300; i++ {
		item = append(item, string(i))
		fmt.Printf("%s", len(item))
		fmt.Printf("Capacity %v\n", cap(item))
	}

}
