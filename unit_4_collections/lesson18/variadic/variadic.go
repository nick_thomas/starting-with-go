package variadic

func NewTerraform(prefix string, worlds ...string) []string {
	newWorlds := make([]string, len(worlds), 20)
	for i := range worlds {
		newWorlds[i] = prefix + " " + worlds[i]
	}
	return newWorlds
}
