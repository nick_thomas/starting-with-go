package main

import (
	"fmt"
	appendexample "lesson18/appendExample"
	"lesson18/capacity"
	slicedump "lesson18/sliceDump"
	threeindexslice "lesson18/threeIndexSlice"
	variadic "lesson18/variadic"
)

func main() {
	appendexample.AppendExample()
	dwarfs1 := []string{"Ceres", "Pluto", "Haumea", "Makemake", "Eris"}
	dwarfs2 := append(dwarfs1, "Orcus")
	dwarfs3 := append(dwarfs2, "Orcus", "Salacia", "Quaoar")
	dwarfs3[1] = "Yahoo"
	slicedump.SliceDump("Original Array", dwarfs1)
	slicedump.SliceDump("Appending one item", dwarfs2)
	slicedump.SliceDump("Appending 3 items", dwarfs3)
	threeindexslice.ThreeIndexSlice()
	twoWorlds := variadic.NewTerraform("New", "Venus", "Mars")
	fmt.Println("New world %s", twoWorlds)
	planets := []string{"Venus", "Mars", "Jupiter"}
	newPlanets := variadic.NewTerraform("Super", planets...)
	fmt.Println("New world %s", newPlanets)
	capacity.Capacity()
}
