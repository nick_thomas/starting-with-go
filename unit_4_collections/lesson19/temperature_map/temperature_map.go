package temperaturemap

import "fmt"

func TemperatureMap() {
	temparute := map[string]int{
		"Earth": 15,
		"Mars":  -65,
	}
	temp := temparute["Earth"]
	fmt.Printf("On Average the Earth is %v C.\n", temp)
	temparute["Earth"] = 16
	temparute["Mars"] = 464
	fmt.Println(temparute)
	if moon, ok := temparute["Mars"]; ok {
		fmt.Println("Found it %v", moon)
	} else {
		fmt.Println("Whoops")
	}
}
