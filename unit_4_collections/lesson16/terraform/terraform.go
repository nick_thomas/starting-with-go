package terraform

import "fmt"

func terraform(planets [8]string) {
	for i := range planets {
		planets[i] = "New " + planets[i]
	}
}

func Terraform() {
	planets := [...]string{
		"Merucry",
		"Venus",
		"Earth",
		"Mars",
		"Jupiter",
		"Saturn",
		"Uranus",
		"Neptune",
	}

	terraform(planets)
	fmt.Println(planets)
}
