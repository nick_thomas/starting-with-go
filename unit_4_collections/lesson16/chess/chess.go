package chess

//“Extend listing 16.8 to display all the chess pieces at their starting positions using the characters kqrbnp for black pieces along the top and uppercase KQRBNP for white pieces on the bottom.
//         Write a function that nicely displays the board.
//         Instead of strings, use [8][8]rune to represent the board. Recall that rune literals are surrounded with single quotes and can be printed with the %c format verb.”
// RBKQBR
// PPPPPP
// pppppp
// rbqkbr

import (
	"fmt"
	"unicode"
)

var board [8][8]rune
var startingSet [8]rune = [8]rune{'R', 'B', 'K', 'Q', 'B', 'R'}

func ChessBoard() {

	for row := range len(board) {
		for column := range board[row] {
			if row == 0 {
				board[0][column] = startingSet[column]
				board[1][column] = 'P'
			}
			if row == len(board)-1 {

				board[len(board)-2][column] = 'p'
				board[len(board)-1][column] = unicode.ToLower(startingSet[column])
			}
		}
	}
	fmt.Printf("%c", board)
}
