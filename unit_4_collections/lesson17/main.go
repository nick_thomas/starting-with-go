package main

import "lesson17/terraform"

func main() {
	planets := []string{"Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune"}
	countries := []string{"India", "Canada", "United States"}
	terraform.Terraform(planets)
	terraform.Terraform(countries)
}
