// “Write a program to terraform a slice of strings by prepending each planet with "New ". Use your program to terraform Mars, Uranus, and Neptune.

// Your first iteration can use a terraform function, but your final implementation should introduce a Planets type with a terraform method, similar to sort.StringSlice.”
package terraform

import "fmt"

func Terraform(s []string) {
	for i, _ := range s {
		s[i] = "New " + s[i]

	}
	fmt.Println(s)

}
