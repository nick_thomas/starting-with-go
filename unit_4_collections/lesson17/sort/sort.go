package sort

import (
	"fmt"
	"sort"
)

func SortMe() {
	planets := []string{"Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune,"}
	sort.StringSlice(planets).Sort()
	fmt.Println(planets)
}
