**How to define an array?**

The following declares an array of string with a length of 8
```var planets [8]string```

---

**What happens to other elements in an array which are not initialized?**

They contain the zero value for their type, for example for strings it be ""

---

**What is composite literal?**

Concise syntax to initialize any composite type with values.

```
dwarfs := [5]string{"Ceres","Pluto","Haumea","Makemake","Eris"}
```

---

**What happens when an array is assigned to a new variable or passing to a function?**

It makes a complete copy of its contents.

---

**What is slicing?**

It creates a window or view into the array.

---

**What is the type of a sliced view?**

slice

---

**Can you index into slices?**

Yes

---

**When slicing what does omitting the first index default to?**

Start of the array

---

**When slicing what does omitting the last index default to?**

The length of the array

---

**How to quickly create a slice?**

```go
dwarfArray[:]
```

---

**What is the type of a slice?**
[]type where type is string,int etc

Behind the scenes Go declares an array and defines a slice

---

**How to append elements to slices?**

Using *append* function

---

**Is append function variadic?**

Yes you can pass multiple elements to append in one go

---

**What is capacity of a slice?**
The ability of a slice of array to grow if array is larger than the slice.

---

**What is length of a slice?**

Number of elements visible through the slice.

---

**What happens when you append a slice?**
Since a slice is a pointer to the underlying array if the capacity of a slice can't hold more items, it copies the contents and creates a new array with larger capacity and points to it, otherwise it appends the same array.

---

**What is three index slicing?**
When appending a slice, if you don't want the original array to overwritten, it is best to supply a third value to slice so as to create a smaller capacity, so incase the slice grows, it is allocated a new array and doesn't overwrite the original.

```go
planets := []string{"Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune"} // length 8 capacity 8
terrestrial := planets[0:4:4] // length 4 capacity 4
```
Appending to terrestrial above would not affect planets


---

**What is preallocating with make sued for?**

When there isn't enough capacity for append, Go allocates a new array, to avoid extra allocations and copies, one can preallocate using make function.
Make function specifies both *length* and *capacity*. *Capacity* is optional

```go
dwarfs := make([]string,0/*length*/,10 /*capcity*/)
dwarfs = append(dwarfs,"Ceres")
```

---

**How to make a variadic function?**

Using the ellipsis `...`.

---

**How to expand a slice?**

Using the ellipsis
```go
planets...
```

---

**How to declare a map?**

map[key type]value type // map[string]int

---

**What happens when you access a key that doesn't exist in the map?**

It results for zero value for the type

---

**How to check if a key exists in a Map in go?**

Using comma,ok syntax

```go
if moon,ok := temperature["Moon"]; ok{
fmt.Printf("On average the moon is %v C\n",moon)
} else {
fmt.Println("Where is the moon?")
}
```

---

**Are Maps in go copied?**

No

---

**How to initialize maps?**

Maps need to be allocated with the ```go make```. Just like slices, initial size can same computer work when maps expand

---

**What is the initial length of when using make?**

It will always be zero

---

**How does make work for a Map?**

Make accepts one or two parameters. The second one preallocates space for a number of keys much like capacity for slices.

```go
temperature := make(map[float64]int,8)
```

---

**How does iteration with range work on maps?**

Iteration with range keyword works similary for slices, arrays and maps. Rather than an index and value, maps provide key and value for each iteration. Go doesn't guarantee order of map,so output will change from one run to another

---

**How to make a map with slice?**
The following is a map with float 64 keys which have values of slice integers

```go
slicedMap = make(map[float64][]int)
```

---

**How does go provide Set?**

Go doesn't have internal set, but we can use map with to create a set.
```go
map = make(map[float64]bool)
```

The above are not sorted, so to sort them we need to convert them back to slice

---
