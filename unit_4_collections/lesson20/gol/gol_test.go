package gol

import (
	"bytes"
	"os"
	"testing"
)

func TestNewUniverse(t *testing.T) {
	universe := NewUniverse(1, 1)
	expected := 1
	if len(universe) != expected && len(universe[0]) != expected {
		t.Errorf("New Universe not created input %v expected %d", len(universe), expected)

	}
}

func TestShowNewUniverse(t *testing.T) {
	universe := NewUniverse(2, 2)
	r, w, _ := os.Pipe()
	originalStdout := os.Stdout
	os.Stdout = w
	universe.Show()
	w.Close()
	os.Stdout = originalStdout

	var buf bytes.Buffer
	_, _ = buf.ReadFrom(r)
	got := buf.String()
	expected := 6
	if len(got) != expected {
		t.Errorf("Output of show is not same from input %v to expected %v", universe, expected)
	}
}
