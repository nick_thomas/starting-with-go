package gol

import (
	"fmt"
	"math"
	"math/rand"
)

const (
	width  = 80
	height = 15
)

type Universe [][]bool

func (u Universe) Show() {
	for i := range u {
		for j := range u[i] {
			if u[i][j] {
				fmt.Printf("*")
				continue
			}
			fmt.Printf(" ")
		}
		fmt.Println()
	}
}

func (u Universe) Alive(index, secondIndex int) {
	u[index][secondIndex] = true

}

// TODO write tests for all the possible functions
func generateRandomNumbers(count, start, end int) []int {
	if start > end || count <= 0 {
		return nil
	}
	numbers := make([]int, count)
	for i := range count {
		numbers[i] = rand.Intn(end-start+1) + start
	}
	return numbers
}

func (u Universe) Seed() {
	count := int(math.Round(float64(len(u)) * .25))
	randomNumbers := generateRandomNumbers(count, 0, len(u)-1)
	countInner := int(math.Round(float64(len(u[0])) * .25))
	randomNumbersInner := generateRandomNumbers(countInner, 0, len(u[0])-1)
	for _, index := range randomNumbers {
		for _, secondIndex := range randomNumbersInner {
			u.Alive(index, secondIndex)
		}
	}

}
func NewUniverse(height, width int) Universe {
	var universe = make(Universe, height)
	for i := range universe {
		universe[i] = make([]bool, width)
	}
	return universe
}
