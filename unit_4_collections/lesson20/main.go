package main

import "lesson20/gol"

func main() {
	u := gol.NewUniverse(10, 10)
	u.Seed()
	u.Show()
}
