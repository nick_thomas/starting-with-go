package main

import (
	"fmt"
	"math"
)

func main() {
	var pi64 = math.Pi
	var pi32 float32 = math.Pi
	sample := 1.0 / 3
	fmt.Println(pi64)
	fmt.Println(pi32)
	fmt.Printf("%0100.2f\n", sample)
}
