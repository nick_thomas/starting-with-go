package alpha

import "fmt"

func CalculateDistance() {
	const lightSpeed = 299792 //km/s
	const secondsPerDay = 86400

	var distance uint64 = 41.3e12
	var earth int = 56e6
	var mars int = 401e6
	fmt.Println("Alpha Centauri is", distance, "km away.")
	fmt.Println("Distance between Earth and Mars ranges from", earth, "kms", mars, "kms")

	days := distance / lightSpeed / secondsPerDay
	fmt.Println("That is", days, "days of travel at light speed.")
}
