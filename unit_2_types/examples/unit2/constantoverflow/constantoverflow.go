package constantoverflow

import "fmt"

func OverflowTest() {
	const distance = 24000000000000000000
	const lightspeed = 299792
	const secondsPerDay = 86400

	const days = distance / lightspeed / secondsPerDay

	fmt.Println("Andromeda Galaxy is", days, "km away.")
}
