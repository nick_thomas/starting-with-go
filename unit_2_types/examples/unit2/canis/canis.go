package canis

import "fmt"

func CalculateMajorDwarf() {
	// “Canis Major Dwarf is the closest known galaxy to Earth at 236,000,000,000,000,000 km from our Sun (though some dispute that
	//         it is a galaxy). Use constants to convert this distance to light years.”
	const lightSpeed = 299792 //km/s
	const secondsPerDay = 86400
	var distance = 236000000000000000
	days := distance / lightSpeed / secondsPerDay
	fmt.Println(distance)
	fmt.Println("That is", days, "days of travel at light speed.")
}
