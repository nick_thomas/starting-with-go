package itoa

import (
	"fmt"
	"strconv"
)

func ITOA() {
	countdown := 10
	str := "Launch in T minus " + strconv.Itoa(countdown) + " seconds."
	fmt.Println(str)
	countdown, err := strconv.Atoi("10")
	if err != nil {
		fmt.Println("Something went wrong")
	}
	fmt.Println(countdown)
}
