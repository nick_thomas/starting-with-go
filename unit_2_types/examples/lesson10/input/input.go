// Write a program that converts strings to Booleans:
// The strings “true”, “yes”, or “1” are true.
// The strings “false”, “no”, or “0” are false.
// Display an error message for any other values.
// Tip
// The switch statement accepts multiple values per case, as covered in lesson 3.”
package input

func StrToBool(value string) bool {
	// TODO with switch
	if value == "true" || value == "yes" || value == "1" {

		return true
	}
	return false
}

func StrToBoolSwitch(value string) bool {
	var boolean_value bool
	switch value {
	case "true":
	case "yes":
	case "1":
		boolean_value = true
	default:
		boolean_value = false
	}
	return boolean_value
}
