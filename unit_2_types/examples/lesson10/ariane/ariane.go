package ariane

import (
	"fmt"
	"math"
)

func TestFlight() {
	var bh float64 = 32767
	if bh < math.MinInt16 || bh > math.MaxInt16 {
		fmt.Println(bh, math.MaxInt16, math.MinInt16)
		bh = 1
	}
	var h = int16(bh)
	fmt.Println(h)
}
