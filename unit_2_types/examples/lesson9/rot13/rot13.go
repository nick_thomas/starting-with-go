package rot13

import "fmt"

func ROT13() {

	message := "uv vagreangvbany fcnpr fgngvba"
	var val byte = 13
	for i := 0; i < len(message); i++ {
		c := message[i]
		if c >= 'a' && c <= 'z' {
			c = c + val
			if c > 'z' {
				c = c - 26
			}
		}
		fmt.Printf("%c", c)
	}
}
