package spanish

import (
	"fmt"
	"unicode/utf8"
)

func NeedAnotherName() {
	question := "¿Cómo estás?"
	fmt.Println(len(question), "bytes")
	fmt.Println(utf8.RuneCountInString(question), "runes")

	for _, c := range question {
		fmt.Printf("%c ", c)
	}
	fmt.Println("Do something")
	for len(question) > 0 {

		c, size := utf8.DecodeRuneInString(question)
		fmt.Printf("First rune: %c %v bytes", c, size)
		fmt.Println()
		question = question[size:]
	}

}
