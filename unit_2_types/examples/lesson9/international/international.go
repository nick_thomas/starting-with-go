package international

import "fmt"

func MessageSpanish() {
	var message = "Hola Estación Espacial Internacional"
	var val rune = 13
	for _, c := range message {
		if c >= 'a' && c <= 'z' {
			c = c + val
			if c > 'z' {
				c = c - 26
			}
		}
		fmt.Printf("%c", c)
	}
}
