package raw

import "fmt"

func RawString() {
	peace := "peace"
	var peace2 string = "peace"
	fmt.Println(peace, peace2)
	fmt.Println("peace be upon you\n upon you be peace")
	fmt.Println(`
    peace be upon you \n
    upon you be peace
    `)
	fmt.Printf("%v is a %[1]T\n", "literal string")
	fmt.Printf("%v is a %[1]T\n", `raw string literal`)
}
