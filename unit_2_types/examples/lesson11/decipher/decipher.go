package decipher

import (
	"fmt"
	"strings"
	"unicode/utf8"
)

// https://www.geeksforgeeks.org/vigenere-cipher/
func Deciphering(cipherText string) {
	key := "GOLANG"
	// get modulo for the calculation
	repeatIndex := utf8.RuneCountInString(cipherText) / utf8.RuneCountInString(key)
	offset := utf8.RuneCountInString(cipherText) % utf8.RuneCountInString(key)
	keyword := strings.Repeat(key, repeatIndex)
	keyword += keyword[0:offset]
	for index, item := range keyword {
		subtractIndex := rune(cipherText[index]) - rune(item) + 26
		decodedIndex := subtractIndex % 26
		output := 'A' + decodedIndex
		fmt.Printf("%c", output)
	}
}
