package cipher

import (
	"strings"
	"unicode/utf8"
)

// “To send ciphered messages, write a program that ciphers plain text using a keyword:

//	plainText := "your message goes here"
//
// keyword := "GOLANG"
//
//	Bonus: rather than write your plain text message in uppercase letters with no spaces, use the strings.Replace and strings.ToUpper functions to remove spaces and uppercase the string before you cipher it.
//
//
//	Once you’ve ciphered a plain text message, check your work by deciphering the ciphered text with the same keyword.
//
//	Use the keyword "GOLANG" to cipher a message and post it to the forums for Get Programming with Go at forums.manning.com/forums/get-programming-with-go.”
func CipherStr() string {
	keyword := "GOLANG"
	plainText := CapitalizeStr("Maybe sometimes I like boogie woogie")
	repeatIndex := utf8.RuneCountInString(plainText) / utf8.RuneCountInString(keyword)
	offset := utf8.RuneCountInString(plainText) % utf8.RuneCountInString(keyword)
	// TODO make sure the cipher length is the same as plaintext for cipher
	cipher := strings.Repeat(keyword, repeatIndex)
	cipher += keyword[0:offset]
	var cipherText []string
	for index, key := range plainText {
		x := (key + rune(cipher[index])) % 26
		x += 'A'
		cipherText = append(cipherText, string(x))
	}
	return strings.Join(cipherText, "")
}

func CapitalizeStr(s string) string {
	newStr := strings.Replace(s, " ", "", -1)
	return strings.ToUpper(newStr)
}
