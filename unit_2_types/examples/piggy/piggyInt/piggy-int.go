package piggyInt

import (
	"fmt"
	"math/rand"
)

// # Function to convert dollars to cents
// def dollars_to_cents(dollars):
//
//	return int(dollars * 100)
func centsToDollars(cents int, div int) int {
	dollars := cents / div
	centsRemainder := cents % div
	return dollars + centsRemainder
}

func PiggyInt() {

	var nickels = 5
	var dimes = 10
	var quarters = 25
	var piggybank = 0
	for {
		rand := rand.Intn(3)
		if piggybank >= 20.00 {
			break
		}
		if rand == 0 {

			piggybank += centsToDollars(nickels, 5)
			continue
		}
		if rand == 1 {

			piggybank += centsToDollars(quarters, 25)
		}
		if rand == 2 {

			piggybank += centsToDollars(dimes, 10)
		}
		fmt.Printf("The running balance %d\n", piggybank)

	}
	fmt.Printf("The final balance %d\n", piggybank)
}
