package piggyFloat

import (
	"fmt"
	"math/rand"
)

func PiggyFloat() {

	var nickels = 0.05
	var dimes = 0.10
	var quarters = 0.25
	var piggybank = 0.00
	for {
		rand := rand.Intn(3)
		if piggybank >= 20.00 {
			break
		}
		if rand == 0 {

			piggybank += nickels
			continue
		}
		if rand == 1 {

			piggybank += quarters
		}
		if rand == 2 {

			piggybank += dimes
		}
		fmt.Printf("The running balance %.2f\n", piggybank)

	}
	fmt.Printf("The final balance %.2f\n", piggybank)
}
