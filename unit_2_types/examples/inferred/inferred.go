package main

import "fmt"

func main() {
	var year int32 = 2018
	fmt.Printf("Type %T for %v\n", year, year)
	sample := "Yahoo"
	sample2 := "Yahoo2"
	fmt.Printf("Type %T for %[1]s, %[2]s %[2]s", sample, sample2)
	fmt.Println()
	var red, green, blue uint8 = 0x00, 0x8d, 0xd5

	fmt.Printf("%x, %x, %x", red, blue, green)
	fmt.Printf("#%02x%02x%02x\n", red, blue, green)

	redi := 3
	redi++
	fmt.Printf("%08b\n", redi)

	var redw int8 = 127
	redw -= -120
	fmt.Println(redw)
}
