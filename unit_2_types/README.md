# Unit 2 Types

**What happens when you declare a variable with Real numbers (decimals)?**

Go compiler infers the type as float64

---

**What happens when you initialize a number with whole number (without decimals)?**

Go compiler will not be able to infer the type, so it needs to be explicit.

---

**What are the two types of floating-point types?**

1.float64 -> 64 bit floating point, uses 8 bit of memory
2. float32 -> 32 bit, uses 4 bit of memory

---

**What is the default value of each type called?**

Zero value

---

**How to format float64 types using PrintF?**
%v lists all of the leading digits
%f formatting verb to specifiy the number of digits.
%f, %.3f (example output 0.333)

---

**How does %f used to format with width and precision**

%4.2f
%4 -> the width of the number which specifies the minimum number of characters to display
.2f -> the precision of the decimal points, like 2 decimal points

---

**What happens when the width is larger than the number of characters needed?**

Printf will pad left with spaces.

---

**What happens when the width is unspecified?**

Printf will use the number of characters necessary to display the value

---

**What is the best way to compare floating numbers?**

Instead of comparing them directly, determine the absolute difference between two numbers and ensure the difference isn't too much

---

**Why does func main throw an error?**

In Golang, each file can only have one main function, to avoid this, you can place your files within their own folders

---

**How many types of whole numbers does Go offer?**

10 different types, collectively called Integers. They do not suffer from accuracy issues of floating point types.

---

**What is the default int type when using inference?**

Signed int type

---

**What are the 8 architecture independent integer types?**

| Type   | Range                                                | Storage |
| int8   | -128 to 127                                          | 8-bit(one byte) |
| uint8  | 0 to 255                                             | |
| int16  | -32768 to 32767                                      | 16-bit (two byte) |
| uint16 | 0 to 65535                                           | |
| int32  | -2,147,483,648 to 2,147,483,647                      | 32-bit(four bytes) |
| uint32 | 0 to 4,294,967,295                                   | |
| int64  | -9,223,372,036,854,775,808 to 9,223,372,036,854,775,807 | 64-bit (eight bytes) |
| uint64 | 0 to 18,446,744,073,709,551,615                      | |

---

**How to inspect a type of a variable, inferred or otherwise?**

with fmt.Printf use the %T qualifier to print out the inferred type.

```
year := 2018
fmt.Printf("Type %T for %[1]",year)
```
---

**How to reuse variables passed via print?**
One can use the [1] to use the first arguement at first position
```
fmt.Printf("%f, %[1]",variable)
```
---
**How does go distinguish decimal from hexadecimal?**
Requires a `0x` prefix

---

**How to print binary representation of a int type?**

Use the format verby %b
```
fmt.Printf("%08b\n",value)
```

---

**How to declare constants with a type?**

```
const distance uint64 = 2400000000000
```

---

**Is every literal value a constant?**

Yes, every literal value, and sized numbers can be directly used.

---

**When are calculations on constants and literal performed by Go?**

They are performed during compilation rather than run time

---

** What are raw string literals useful for?**

They can span multiple lines of code and in output use the same tab spacing

---

**How to ensure both literal and raw string literals output as formatted strings?**

using %v
```
	fmt.Printf("%v is a %[1]T\n", "literal string")
	fmt.Printf("%v is a %[1]T\n", `raw string literal`)
```

---

**What is a rune in Go?**

It is used to represent a single Unicode code point. It is an alias for the `int32` type

---

**What is a byte in an alias for in Go?**

It is used to represent `uint8` type.

---

**How to display the characters instead of the numeric representations?**

Use `%c` format verb

---

**Are strings in Go immutable?**

Yes, so a variable with string cannot be modfied.

---

**What are Strings in Go encoded as?**

String in Go are encoded as UTF-8

---

## Lesson 10 Types

**When are type conversions required?**

- Between unsigned and signed integer
- Between types of different sizes

---

**Name two functions that can convert an integer to string?**

`Itoa` & `Sprintf`
