// “Implement a drawTable function that takes a first-class function as a parameter and calls it to get data for each row drawn. Passing a different fnction to drawTable should result in different data being displayed.”

package main

import "fmt"

func drawPad() {

	fmt.Println("====================")
}

func drawTable(draw func()) {
	draw()
}

func main() {
	drawTable(drawPad)
	fmt.Println("| *C          |        *F |")
	drawTable(drawPad)
}
