package sensor

import (
	"fmt"
	"lesson14/calibrate"
	"math/rand"
	"time"
)

type sensor func() calibrate.Kelvin

func RealSensor() calibrate.Kelvin {
	return 0
}

func FakeSensor() calibrate.Kelvin {
	return calibrate.Kelvin(rand.Intn(151) + 150)
}

func MeasureTemperature(samples int, sensor sensor) {
	for i := 0; i < samples; i++ {
		k := sensor()
		fmt.Printf("%v K\n", k)
		time.Sleep(time.Second)
	}
}

type getRowFn func(row int) (string, string)

func drawTable(rows int, getRow getRowFn) int {
	return 2
}
