package calibrate

type Kelvin float64

type sensor func() Kelvin

func RealSensor() Kelvin {
	return 0
}

func Calibrate(s sensor, offset Kelvin) sensor {
	return func() Kelvin {
		return s() + offset
	}
}
