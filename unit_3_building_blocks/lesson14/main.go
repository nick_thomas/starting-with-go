package main

import (
	"fmt"
	"lesson14/calibrate"
	"lesson14/sensor"
)

func main() {
	// sensor.MeasureTemperature(3, sensor.FakeSensor)
	// masquerade.Mask()
	// funcvar.FuncVar()
	sensor := calibrate.Calibrate(sensor.FakeSensor, 5)
	fmt.Println(sensor())
	fmt.Println(sensor())

}
