package funcvar

import "fmt"

func FuncVar() {
	f := func(message string) {
		fmt.Println(message)
	}
	f("Go to party")
}
