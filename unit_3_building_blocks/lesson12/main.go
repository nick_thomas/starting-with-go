package main

import (
	"fmt"
	"lesson12/methods"
)

func main() {
	c := methods.Celsius(0)
	o := methods.CelsiusToFahrenheit(c)
	fmt.Printf("The value is %.2f", o)
	k := methods.Kelvin(1)
	f := methods.KelvinToFahrenheit(k)
	fmt.Printf("The value is %.2f", f)
}
