package methods

type Celsius = float64
type Fahrenheit = float64
type Kelvin = float64

func CelsiusToFahrenheit(celsius Celsius) Fahrenheit {
	return celsius*(9/5) + 32
}
func FahrenheitToCelsius(fahrenheit Fahrenheit) Celsius {
	return (fahrenheit - 32) * (5 / 9)
}
func CelsiusToKelvin(celsius Celsius) Kelvin {
	return celsius + 273.15
}
func KelvinToCelsius(kelvin Kelvin) Celsius {
	return kelvin - 273.15
}

func FahrenheitToKelvin(fahrenheit Fahrenheit) Kelvin {
	return (5 / 9) * (fahrenheit + 459.67)
}
func KelvinToFahrenheit(kelvin Kelvin) Fahrenheit {
	return 1.8*(kelvin-273) + 32
}
