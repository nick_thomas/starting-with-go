package kelvin

import "lesson12/celsius"

func KelvinToCelsius(k float64) float64 {
	k -= 273.15
	return k
}

func KelvinToFahrenheit(kelvin float64) float64 {
	celsiusVal := celsius.CelsiusToFahrenheit(KelvinToCelsius(kelvin))
	return celsiusVal
}
