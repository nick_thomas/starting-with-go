**What does a function delcaration in Go look like?**

```
func    Intn          (n               int)            int
keyword function name  parameter name  parameter type  result type
```

---

**How should functions,variables and other identifiers begin to be exported?**

They need to begin with *uppercase* letters to become exported and available to other packages

---

**What is the shortcut way of declaring multiple parameters with same type?**

`func Unix(sec, nsec int64) Time`, here both sec and nsec have the same type `int 64`

---

**How to setup multiple returns for a function?**

`func Atoi(s string) (i int, err error)` is the same as `func Atoi(s string) (int,error)`

---

**How to declare a variadic function with an example?**

In the following `func Println(a ...interface{}) (n int, err error)`, ellipsis(...) indicates a variadic function. The parameter a is a collection of arguments passed to a function.
It means we can pass `Println` any number of arguements of any type.

---

**How to declare a newtype in Go?**

``` type celsius float64 ```
---

**How to export a new type?**

Same as function, name it with a capital letter.

---

**How to convert from one type to another?**

If the type has the same base type, then wrapping it like following converts it

```
var warmUp float64 = 10
temperatre += celsius(warmUp)
```

---

**How to add methods to a type?**

```
type kelvin float64
func (k kelvin) celsius() celsius {
return celsius(k-273.15)
}
```

---

**How are methods in Go invoked?**

Using dot notation, which looks like calling a functio nfrom another package

---

**What are functions in Go?**

Functions are first-class, they work in all the places that integers, strings and other types work.

---

**Does Go allow passing functions to another functions?**

Yes. Variables can refer to functions and variables can be passed to functions.

---

**What are anonymous functions?**

They are called function literal in Go. A function without a name. Unlike regular functions, function literals are closures because they keep references to variables in surrounding scope

```go
var f = func(){/*anonymous function*/}

f := func(){/* anonymous function */}
f()

func(){/*anonymous function */}()

```
---

**How does closure enclose the variable in scope?**

Closure keeps a reference to surrounding variables. Changing those variables are reflected. Especially important for loops
